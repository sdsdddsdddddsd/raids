package ca.landonjw.remoraids.api.commands.arguments.parsers;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.management.PlayerList;
import net.minecraftforge.fml.common.FMLCommonHandler;

import javax.annotation.Nonnull;
import java.util.Optional;

public class OnlinePlayerArgumentParser implements IArgumentParser<EntityPlayerMP> {

    @Override
    public Optional<EntityPlayerMP> parse(@Nonnull String argument) {
        PlayerList playerList = FMLCommonHandler.instance().getMinecraftServerInstance().getPlayerList();
        return Optional.ofNullable(playerList.getPlayerByUsername(argument));
    }

}
