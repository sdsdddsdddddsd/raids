package ca.landonjw.remoraids.api.commands.arguments;

import ca.landonjw.remoraids.api.messages.placeholders.IParsingContext;

import javax.annotation.Nonnull;
import java.util.List;

public interface IRaidsArgument {

    List<String> getTokens();

    void interpret(@Nonnull String value, @Nonnull IParsingContext context) throws IllegalArgumentException;

}