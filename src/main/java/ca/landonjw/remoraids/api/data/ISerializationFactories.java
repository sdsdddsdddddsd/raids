package ca.landonjw.remoraids.api.data;

import ca.landonjw.remoraids.api.battles.IBattleRestraint;
import ca.landonjw.remoraids.api.rewards.IReward;
import ca.landonjw.remoraids.api.rewards.contents.IRewardContent;

import javax.annotation.Nonnull;
import java.util.Optional;

public interface ISerializationFactories {

    <T> void registerFactory(@Nonnull Class<T> clazz, @Nonnull SerializationFactory<T> factory);

    <T> Optional<SerializationFactory<T>> getFactory(@Nonnull Class<T> clazz);

    SerializationFactory<IRewardContent> getRewardContentFactory();

    SerializationFactory<IReward> getRewardFactory();

    SerializationFactory<IBattleRestraint> getRestraintFactory();

}
