package ca.landonjw.remoraids.implementation.commands.create;

import ca.landonjw.remoraids.api.commands.arguments.IRaidsArgument;
import ca.landonjw.remoraids.implementation.commands.create.arguments.*;
import com.pixelmonmod.pixelmon.entities.pixelmon.stats.StatsType;

import javax.annotation.Nonnull;

public enum CreateCommandArguments {
    LOCATION(new LocationArgument()),
    WORLD(new WorldArgument()),
    ROTATION(new RotationArgument()),
    RESPAWN_AMOUNT(new RespawnAmountArgument()),
    RESPAWN_TIME(new RespawnTimeArgument()),
    //    PERSISTENT(new PersistentArgument()), //TODO: This is implemented, but disabled for the 1.1.0 release.
    SIZE(new SizeArgument()),
    MOVES(new MovesArgument()),
    STATS(new FullStatsArgument()),
    HP(new SingleStatArgument(StatsType.HP)),
    ATTACK(new SingleStatArgument(StatsType.Attack)),
    DEFENCE(new SingleStatArgument(StatsType.Defence)),
    SPECIAL_ATTACK(new SingleStatArgument(StatsType.SpecialAttack)),
    SPECIAL_DEFENCE(new SingleStatArgument(StatsType.SpecialDefence)),
    SPEED(new SingleStatArgument(StatsType.Speed)),
    REWARD(new RewardArgument()),
    CAPACITY_RESTRAINT(new CapacityRestraintArgument()),
    COOLDOWN_RESTRAINT(new CooldownRestraintArgument()),
    NO_REBATTLE_RESTRAINT(new NoRebattleRestraintArgument());

    private IRaidsArgument argument;

    CreateCommandArguments(@Nonnull IRaidsArgument argument) {
        this.argument = argument;
    }

    public IRaidsArgument getArgument() {
        return argument;
    }
}
